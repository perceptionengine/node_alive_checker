#!/usr/bin/env python

from __future__ import print_function

import roslib
roslib.load_manifest('node_alive_checker')

import rospy
import rosnode
import diagnostic_updater
import diagnostic_msgs
import ast

class NodeAliveChecker:
    def __init__(self):
        rospy.init_node("node_alive_checker", anonymous=False)

        self.__nodes = rospy.get_param('~nodes', ['record1'])
        try:
            self.__nodes = ast.literal_eval(self.__nodes)
        except:
            rospy.logerr("Invalid 'nodes' param")
        print(self.__nodes)
        self.__diagnostics_topic = rospy.get_param('~diagnostics_topic', '/diagnostics')
        self.__rate = rospy.get_param('~rate', 5)
        self.__updater = diagnostic_updater.Updater()
        self.__updater.setHardwareID("node_alive_checker")
        self.__updater.add("node_ping", self.check_nodes)
        if self.__rate <= 0:
            self.__rate = 5
        rospy.loginfo("Running")
        while not rospy.is_shutdown():
            rospy.sleep(self.__rate)
            self.__updater.update()

    def check_nodes(self, diag_stat):
        status = diagnostic_msgs.msg.DiagnosticStatus.OK
        error_node = ""
        try:
            for n in self.__nodes:
                if not rosnode.rosnode_ping(n, 1, skip_cache=True, verbose=False):
                    status = diagnostic_msgs.msg.DiagnosticStatus.ERROR
                    error_node = error_node + " " + n + " not alive."
        except rosnode.ROSNodeIOException:
            status = diagnostic_msgs.msg.DiagnosticStatus.ERROR
            error_node = "Network Communication Error"
        except:
            status = diagnostic_msgs.msg.DiagnosticStatus.ERROR
            error_node = "Error while checking nodes"
        if status == diagnostic_msgs.msg.DiagnosticStatus.OK:
            error_node = "All nodes working"
        diag_stat.summary(status, error_node)


if __name__ == "__main__":
    node = NodeAliveChecker()
